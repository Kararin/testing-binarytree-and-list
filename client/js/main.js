var TR = {};
document.addEventListener('DOMContentLoaded', function() {
    'use strict';
    var tree = new TR.Tree();

    tree.addValue(2);
    tree.addValue(5);
    tree.addValue(9);
    tree.addValue(0);
    tree.addValue(-2);
    tree.addValue(6);

    console.log(tree.getTree());
    console.log(tree.findValue(2));
    console.log(tree.findMinValue());
    console.log(tree.deleteValue(6));
});
