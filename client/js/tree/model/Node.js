(function (This) {
	This.Node = function (data) {
		return {
			data: data,
			left: {},
			right: {}
		};
	};
})(TR);