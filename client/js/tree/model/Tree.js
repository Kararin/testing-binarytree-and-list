(function(This) {
    'use strict';
    This.Tree = function(data) {
        var tree = new This.Node();

        this.addValue = function(newValue) {
            addValueToTree(newValue, tree);
        };

        function addValueToTree(newValue, root) {
            if (!root.data) {
                root.data = newValue;
                return;
            } else {
                if (newValue < root.data) {
                    addValueToTree(newValue, root.left || (root.left = new This.Node()));
                } else {
                    if (newValue > root.data) {
                        addValueToTree(newValue, root.right || (root.right = new This.Node()));
                    }
                }
            }
            return;
        }

        this.findValue = function(value) {
            return findValueInTree(value, tree);
        };

        function findValueInTree(value, root) {
            if (root.data === value) {
                return root.data;
            } else {
                if (value < root.value) {
                    return findValueInTree(value, root.left);
                } else {
                    return findValueInTree(value, root.right);
                }
            }
        }

        this.getTree = function() {
            return  _.clone(tree);
        };

        this.findMinValue = function() {
            return findMinValueInTree(tree);
        };

        function findMinValueInTree(root) {
            if (!root.left) {
                return root.data;
            } else {
                return findMinValueInTree(root.left);
            }
        }

        this.deleteValue = function(value) {
            deleteValueFromTree(value, tree);
            return this.getTree();
        };

        function deleteValueFromTree(value, root) {
            if (root) {
            var values = {
                'left': (root.left) && (root.left.data),
                'right': (root.right) && (root.right.data)
            },
            key = _.invert(values)[value];
            if (key) {
                root[key] = findNewNode(value, root[key]);
            }
            // if (value === root.left.data) {
            //     root.left = findNewNode(value, root.left);
            // } else {
            //     if (value === root.right.data) {
            //         root.right = findNewNode(value, root.right);
                 //} 
                else {
                    if (value < root.data && root.left) {
                        deleteValueFromTree(root.left);
                    } else {
                        if (value > root.data && root.right) {
                            deleteValueFromTree(root.right);
                        }
                    }
                }
            }l
            return;
        }

        function findNewNode(value, root) {
            var minNode;

            if (root.left && root.right) {
                minNode = findMinValue(root);
                deleteValueFromTree(minNode);
                return minNode;
            } else {
                if (root.left) {
                    return root.left;
                } else {
                    if (root.right) {
                        return root.right;
                    }
                }
            }

            return null;
        }

        return this;
    };
})(TR);
